# Tic Tac Toe

My implementation of the famous Tic Tac Toe game in pure C++11 and STL without using any libraries.

### Key features:

* Minimax algorithm for turn-based AI
* Object-Oriented Design using design patterns: Observer, Composite, State, Singleton, Builder
* Advanced menu system with tree data structure
