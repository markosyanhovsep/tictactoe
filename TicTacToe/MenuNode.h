#pragma once

#include <vector>
#include <map>
#include <memory>

class SubMenu;

class MenuNode
{
public:
	MenuNode(std::string&& _name);
	MenuNode(const std::string& _name);

	//Accessors
	SubMenu* GetParent() const { return parent; };
	void SetParent(SubMenu* _parent) { parent = _parent; };

	//virtual function
	virtual void Action() = 0;
	virtual ~MenuNode() {};

public:

	SubMenu* parent;
	std::string name;

};

