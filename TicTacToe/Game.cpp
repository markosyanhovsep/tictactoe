#include "Game.h"
#include "SubMenu.h"
#include "MenuBuilder.h"
#include "MenuSwitcher.h"

#define NOMINMAX
#include <Windows.h>

#include <iostream>
#include <algorithm>

Game::Game()
{
	ClearBoard();
}

Game::~Game()
{
}

void Game::NewGame(char side)
{
	ClearBoard();
	player = side;
	ai = (side == 'X') ? 'O' : 'X';
	scores[player] = 10;
	scores[ai] = -10;
	state = GameState::PLAYING;
}

void Game::Resume()
{
	state = GameState::PLAYING;
}

void Game::Restart()
{
	ClearBoard();
	state = GameState::PLAYING;
}

void Game::ClearBoard()
{
	for (auto& row : board)
	{
		for (auto& col : row)
		{
			col = '_';
		}
	}
}

void Game::Draw()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	for (const auto& row : board)
	{
		for (const auto& col : row)
		{
			SetConsoleTextAttribute(hConsole, static_cast<int>(colorMap[col]));
			std::cout << "\t" << col << "\t";
		}
		std::cout << "\n\n";
	}
	SetConsoleTextAttribute(hConsole, static_cast<int>(colorMap['T']));
	std::cout << "\n\n";
	std::cout << "Select empty cell (1-9)" << std::endl;
	std::cout << "0 for pause menu: ";
}

void Game::Exit()
{
	active = false;
}

bool Game::PlayerAction(int action)
{
	if (action < 1 || action > 9)
	{
		return false;
	}
	int row = (action - 1) / 3;
	int col = (action - 1) % 3;
	if (board[row][col] != '_')
	{
		return false;
	}
	board[row][col] = player;
	auto winner = CheckWinner();
	if (winner != '_')
	{
		std::string s = "winner: ";
		s += winner;
		GameOver(std::move(s));
		return true;
	}
	if (!IsMovesLeft())
	{
		GameOver("Draw");
		return true;
	} 
	AIAction();
	return true;
}

void Game::AIAction()
{
	Move AIMove;
	switch (difficulty)
	{
	case Difficulty::EASY:
		AIMove = FindRandomMove();
		break;
	case Difficulty::HARD:
		AIMove = FindBestMove();
		break;
	case Difficulty::MEDIUM:
		auto rand = std::rand() % 3;
		AIMove = (rand == 0) ? FindBestMove() : FindRandomMove();
		break;
	}
	board[AIMove.row][AIMove.col] = ai;
	auto winner = CheckWinner();
	if (winner != '_')
	{
		std::string s = "winner: ";
		s += winner;
		GameOver(std::move(s));
	}
}

void Game::GameOver(std::string&& message)
{
	state = GameState::STANDBY;
	MenuSwitcher::GetInstance().SetActiveMenu("gameover");
	MenuSwitcher::GetInstance().GetActiveMenu()->SetCaption(std::move(message));
}

Move Game::FindRandomMove()
{
	auto emptyCells = GetEmptyCells();
	auto number = std::rand() % emptyCells.size();
	Move move;
	move.row = emptyCells[number] / board.size();
	move.col = emptyCells[number] % board.size();
	return move;
}

std::vector<int> Game::GetEmptyCells()
{
	std::vector<int> cells;
	for (auto row = 0; row < board.size(); row++)
	{
		for (auto col = 0; col < board.size(); col++)
		{
			if (board[row][col] == '_')
			{
				auto number = row * board.size() + col;
				cells.push_back(number);
			}
		}
	}
	return cells;
}

bool Game::IsMovesLeft()
{
	return GetEmptyCells().size() != 0;
}

Move Game::FindBestMove()
{
	int bestScore = INT_MAX;
	Move bestMove;
	bestMove.row = -1;
	bestMove.col = -1;
	for (auto row = 0; row < board.size(); row++)
	{
		for (auto col = 0; col < board.size(); col++)
		{
			if (board[row][col] == '_')
			{
				board[row][col] = ai;
				int moveScore = Minimax(0, true);
				board[row][col] = '_';
				if (moveScore < bestScore)
				{
					bestScore = moveScore;
					bestMove.row = row;
					bestMove.col = col;
				}
			}
		}
	}
	return bestMove;
}

char Game::CheckWinner()
{
	//checking rows
	for (auto row = 0; row < board.size(); row++)
	{
		if (board[row][0] == board[row][1] && board[row][1] == board[row][2])
		{
			if (board[row][0] != '_')
			{
				return board[row][0];
			}
		}
	}
	//checking cols
	for (auto col = 0; col < board.size(); col++)
	{
		if (board[0][col] == board[1][col] && board[1][col] == board[2][col])
		{
			if (board[0][col] != '_')
			{
				return board[0][col];
			}
		}
	}
	//checking diagonals
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2])
	{
		if (board[0][0] != '_')
		{
			return board[0][0];
		}

	}
	if (board[0][2] == board[1][1] && board[1][1] == board[2][0])
	{
		if (board[0][2] != '_')
		{
			return board[0][2];
		}
	}
	return '_';
}

int Game::GetHeuristicScore()
{
	auto winner = CheckWinner();
	return scores[winner];
}

int Game::Minimax(int depth, bool isMax)
{
	int score = GetHeuristicScore();
	if(score != 0)
	{
		return score;
	}
	if (!IsMovesLeft())
	{
		return 0;
	}
	if (isMax)
	{
		int bestScore = INT_MIN;
		for (auto& row : board)
		{
			for (auto& col : row)
			{
				if (col == '_')
				{
					col = player;
					bestScore = std::max(bestScore, Minimax(depth + 1, !isMax));
					col = '_';
				}
			}
		}
		return bestScore;
	}
	else
	{
		int bestScore = INT_MAX;
		for (auto& row : board)
		{
			for (auto& col : row)
			{
				if (col == '_')
				{
					col = ai;
					bestScore = std::min(bestScore, Minimax(depth + 1, !isMax));
					col = '_';
				}
			}
		}
		return bestScore;
	}
}
