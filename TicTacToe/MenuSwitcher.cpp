#include "MenuSwitcher.h"

MenuSwitcher::MenuSwitcher()
{
}

MenuSwitcher& MenuSwitcher::GetInstance()
{
	static MenuSwitcher instance;
	return instance;
}

MenuSwitcher::~MenuSwitcher()
{
	for (auto& menu : menus)
	{
		delete menu.second;
	}
}

void MenuSwitcher::AddMenu(const std::string& name, SubMenu* menu)
{
	menus[name] = menu;
}

void MenuSwitcher::SetActiveMenu(const std::string& name)
{
	activeMenu = menus[name];
}

void MenuSwitcher::SetActiveMenu(SubMenu* menu)
{
	activeMenu = menu;
}

SubMenu* MenuSwitcher::GetActiveMenu()
{
	return activeMenu;
}
