#pragma once

#include "MenuNode.h"

#include <functional>

class MenuOption : public MenuNode
{
public:
	
	MenuOption(std::string&& _name, std::function<void()> _OnSelected);
	MenuOption(const std::string& _name, std::function<void()> _OnSelected);
	virtual ~MenuOption();

	virtual void Action() override;

private:

	std::function<void()> OnSelected;

};

