#include "SubMenu.h"
#include "MenuOption.h"
#include "MenuBuilder.h"
#include "MenuSwitcher.h"

#include <iostream>
#include <string>

SubMenu::SubMenu(std::string&& _name) : MenuNode(std::move(_name))
{
}

SubMenu::~SubMenu()
{
	for (auto& child : children)
	{
		delete child;
	}
}

void SubMenu::Action()
{
	MenuSwitcher::GetInstance().SetActiveMenu(this);
}

MenuNode* SubMenu::AddChild(MenuNode* node)
{
	children.push_back(node);
	children.back()->SetParent(this);
	return children.back();
}

void SubMenu::SetCaption(std::string&& _caption)
{
	caption = std::move(_caption);
}

void SubMenu::Draw()
{
	std::cout << name << std::endl;
	std::cout << "\n";
	std::cout << caption << std::endl;
	std::cout << "\n";
	for (auto i = 0; i < children.size(); i++)
	{
		std::cout << i << ". " << children[i]->name << std::endl;
	}
	std::cout << "\n";
	std::cout << "Select (0-" << children.size() - 1 << "): ";
}
