#pragma once

class Game;
class SubMenu;
class BackOption;

class MenuBuilder
{
public:
	
	MenuBuilder();
	~MenuBuilder();

	SubMenu* BuildMainMenu(Game* game);

	SubMenu* BuildPauseMenu(Game* game);

	SubMenu* BuildGameOverMenu(Game* game);

private:

	SubMenu* BuildOptionsMenu(Game* game);

	SubMenu* BuildPickSideMenu(Game* game);

	SubMenu* BuildColorMenu(Game* game);

	SubMenu* BuildColorPickerMenu(Game* game, char type);

	SubMenu* BuildDifficultyMenu(Game* game);

};

