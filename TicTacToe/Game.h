#pragma once

#include <string>
#include <map>
#include <array>
#include <vector>

class SubMenu;

enum class ColorValue : char
{
	red = 12,
	yellow = 14,
	blue = 9,
	white = 15,
	green = 2,
	magenta = 5
};

enum class GameState : char
{
	STANDBY,
	PLAYING,
	PAUSED
};

enum class Difficulty : char
{
	EASY,
	MEDIUM,
	HARD
};

struct Move
{
	int row;
	int col;
};

class Game
{
public:
	
	Game();
	~Game();

	void Exit();
	void NewGame(char side);
	void Restart();
	void Resume();

	void ClearBoard();
	void Draw();
	bool PlayerAction(int action);

	//Setters
	void SetDifficulty(Difficulty _difficulty) { difficulty = _difficulty; }
	void SetColor(char type, const std::string& color) {colorMap[type] = colors[color]; }
	void SetState(GameState _state) { state = _state; };

	//Getters
	std::map<std::string, ColorValue>& GetColors() { return colors; };
	std::map<std::string, Difficulty>& GetDifficulties() { return difficulties; };
	GameState GetState() { return state; };
	ColorValue GetTextColor() { return colorMap['T']; };
	bool IsActive() { return active; };

private:
	
	//finds best move for AI using minimax algorithm (hard mode)
	Move FindBestMove();
	//gives heuristic score for minimax algorithm
	int GetHeuristicScore();
	//implementation of minimax algorithm for turn-based AI
	int Minimax(int depth, bool isMax);
	//finds random move for AI (easy mode)
	Move FindRandomMove();
	
	bool IsMovesLeft();
	std::vector<int> GetEmptyCells();
	void AIAction();
	char CheckWinner();
	void GameOver(std::string&& message);

private:	
	
	Difficulty difficulty = Difficulty::MEDIUM;
	
	char player = 'X';
	char ai = 'O';

	GameState state = GameState::STANDBY;
	bool active = true;

	std::array<std::array<char, 3>, 3> board;

	std::map<char, int> scores = { {'_', 0} };

	std::map<std::string, ColorValue> colors = { {"red", ColorValue::red},
		{"yellow", ColorValue::yellow}, {"blue", ColorValue::blue},
		{"white", ColorValue::white}, {"green", ColorValue::green},
		{"magenta", ColorValue::magenta} };

	std::map<char, ColorValue> colorMap = { {'X', ColorValue::white},
											{'O', ColorValue::white},
											{'T', ColorValue::white},
											{'_', ColorValue::white} };

	std::map<std::string, Difficulty> difficulties = { {"easy", Difficulty::EASY},
													   {"medium", Difficulty::MEDIUM},
													   {"hard", Difficulty::HARD} };

};

