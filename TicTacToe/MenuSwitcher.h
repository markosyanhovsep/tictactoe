#pragma once

#include <vector>
#include <map>

class SubMenu;

class MenuSwitcher
{
public:
	~MenuSwitcher();
	void AddMenu(const std::string& name, SubMenu* menu);
	void SetActiveMenu(const std::string& name);
	void SetActiveMenu(SubMenu* menu);
	SubMenu* GetActiveMenu();

	static MenuSwitcher& GetInstance();

private:
	MenuSwitcher();
	MenuSwitcher(const MenuSwitcher&) = delete;
	MenuSwitcher(const MenuSwitcher&&) = delete;

	std::map<std::string, SubMenu*> menus;
	SubMenu* activeMenu = nullptr;
};

