#include "BackOption.h"

#include "SubMenu.h"
#include "MenuBuilder.h"
#include "MenuSwitcher.h"

BackOption::BackOption():MenuNode("Back")
{
}


BackOption::~BackOption()
{
}

void BackOption::Action()
{
	MenuSwitcher::GetInstance().SetActiveMenu(this->parent->parent);
}
