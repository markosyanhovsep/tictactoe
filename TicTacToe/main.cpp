#include "MenuNode.h"
#include "MenuOption.h"
#include "BackOption.h"
#include "SubMenu.h"
#include "MenuBuilder.h"
#include "MenuSwitcher.h"
#include "Game.h"

#include <Windows.h>

#include <iostream>
#include <map>

int main()
{
	Game* game = new Game();
	
	//setuping console
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, (int)game->GetTextColor());
	PCONSOLE_FONT_INFOEX lpConsoleCurrentFontEx = new CONSOLE_FONT_INFOEX();
	lpConsoleCurrentFontEx->cbSize = sizeof(CONSOLE_FONT_INFOEX);
	GetCurrentConsoleFontEx(hConsole, 0, lpConsoleCurrentFontEx);
	lpConsoleCurrentFontEx->dwFontSize.X = 20;
	lpConsoleCurrentFontEx->dwFontSize.Y = 20;
	SetCurrentConsoleFontEx(hConsole, 0, lpConsoleCurrentFontEx);
	
	//creating menu
	MenuBuilder menuBuilder;
	auto& menuSwitcher = MenuSwitcher::GetInstance();	
	menuSwitcher.AddMenu("main", menuBuilder.BuildMainMenu(game));
	menuSwitcher.AddMenu("pause", menuBuilder.BuildPauseMenu(game));
	menuSwitcher.AddMenu("gameover", menuBuilder.BuildGameOverMenu(game));
	menuSwitcher.SetActiveMenu("main");
	menuSwitcher.GetActiveMenu()->Draw();
	
	//starting game loop
	while (game->IsActive())
	{
		//handling user input
		int action = 0;
		std::cin >> action;
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(1000, '\n');
			std::cout << "please, enter valid number: ";
			continue;
		}
		//updating state
		if (game->GetState() == GameState::PLAYING)
		{
			if (action == 0)
			{
				menuSwitcher.SetActiveMenu("pause");
				game->SetState(GameState::PAUSED);
			}
			else
			{
				if (!game->PlayerAction(action))
				{
					std::cout << "Please, enter valid number: ";
					continue;
				}	
			}
		}
		else
		{
			if (!menuSwitcher.GetActiveMenu()->IsValidAction(action))
			{
				std::cout << "Please, enter valid number: ";
				continue;
			}	
			menuSwitcher.GetActiveMenu()->GetChild(action)->Action();
		}
		
		//rendering
		system("cls");
		SetConsoleTextAttribute(hConsole, static_cast<int>(game->GetTextColor()));
		(game->GetState() == GameState::PLAYING) ? game->Draw() : 
			menuSwitcher.GetActiveMenu()->Draw();
	}
	delete game;
	return 0;
}