#include "MenuOption.h"

MenuOption::MenuOption(std::string&& _name, std::function<void()> _OnSelected) 
	: MenuNode(std::move(_name)), OnSelected(_OnSelected)
{
}

MenuOption::MenuOption(const std::string& _name, std::function<void()> _OnSelected)
	: MenuNode(_name), OnSelected(_OnSelected)
{
}

MenuOption::~MenuOption()
{
}

void MenuOption::Action()
{
	OnSelected();
}
