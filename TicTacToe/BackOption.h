#pragma once
#include "MenuNode.h"

class BackOption : public MenuNode
{
public:
	BackOption();
	virtual ~BackOption();

	virtual void Action() override;
};

