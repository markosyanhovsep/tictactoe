#include "MenuBuilder.h"

#include "MenuNode.h"
#include "SubMenu.h"
#include "MenuOption.h"
#include "BackOption.h"
#include "MenuSwitcher.h"
#include "Game.h"

MenuBuilder::MenuBuilder()
{
}

MenuBuilder::~MenuBuilder()
{
}

SubMenu* MenuBuilder::BuildMainMenu(Game* game)
{
	SubMenu* mainMenu = new SubMenu("Main Menu");
	mainMenu->AddChild(BuildPickSideMenu(game));
	mainMenu->AddChild(BuildOptionsMenu(game));
	mainMenu->AddChild(new MenuOption("Exit", [game]() { game->Exit(); }));
	return mainMenu;
}

SubMenu* MenuBuilder::BuildPauseMenu(Game* game)
{
	SubMenu* pauseMenu = new SubMenu("Pause Menu");
	pauseMenu->AddChild(new MenuOption("Resume", [game]() { game->Resume(); }));
	pauseMenu->AddChild(new MenuOption("Restart", [game]() { game->Restart(); }));
	pauseMenu->AddChild(BuildOptionsMenu(game));
	pauseMenu->AddChild(new MenuOption("Exit to Main Menu", []() {
		MenuSwitcher::GetInstance().SetActiveMenu("main");
	}));
	return pauseMenu;
}

SubMenu* MenuBuilder::BuildGameOverMenu(Game* game)
{
	SubMenu* gameOverMenu = new SubMenu("Game Over");
	gameOverMenu->AddChild(new MenuOption("Restart", [game]() { game->Restart(); }));
	gameOverMenu->AddChild(new MenuOption("Exit to Main Menu", []() {
		MenuSwitcher::GetInstance().SetActiveMenu("main");
	}));
	return gameOverMenu;
}

SubMenu* MenuBuilder::BuildOptionsMenu(Game* game)
{
	SubMenu* OptionsMenu = new SubMenu("Options Menu");
	OptionsMenu->AddChild(BuildColorMenu(game));
	OptionsMenu->AddChild(BuildDifficultyMenu(game));
	OptionsMenu->AddChild(new BackOption());
	return OptionsMenu;
}

SubMenu* MenuBuilder::BuildPickSideMenu(Game* game)
{
	SubMenu* PickSideMenu = new SubMenu("New Game");
	PickSideMenu->SetCaption("Pick side");
	PickSideMenu->AddChild(new MenuOption("X", [game]() { game->NewGame('X'); }));
	PickSideMenu->AddChild(new MenuOption("O", [game]() { game->NewGame('O'); }));
	PickSideMenu->AddChild(new BackOption());
	return PickSideMenu;
}

SubMenu* MenuBuilder::BuildColorMenu(Game* game)
{
	SubMenu* ColorMenu = new SubMenu("Color");
	ColorMenu->AddChild(BuildColorPickerMenu(game, 'T'));
	ColorMenu->AddChild(BuildColorPickerMenu(game, 'X'));
	ColorMenu->AddChild(BuildColorPickerMenu(game, 'O'));
	ColorMenu->AddChild(new BackOption());
	return ColorMenu;
}

SubMenu* MenuBuilder::BuildColorPickerMenu(Game* game, char type)
{
	std::string name = (type == 'T')? "Text" : std::string(1, type);
	name += " color menu";
	SubMenu* TextColorMenu = new SubMenu(std::move(name));
	for (const auto& color : game->GetColors())
	{
		TextColorMenu->AddChild(new MenuOption(color.first, [game, &color, type, TextColorMenu]() {
			game->SetColor(type, color.first); 
			TextColorMenu->SetCaption("Selected: " + color.first);
		}));
	}
	TextColorMenu->AddChild(new BackOption());
	return TextColorMenu;
}

SubMenu* MenuBuilder::BuildDifficultyMenu(Game* game)
{
	SubMenu* DifficultyMenu = new SubMenu("Difficulty");
	for (const auto& diff : game->GetDifficulties())
	{
		DifficultyMenu->AddChild(new MenuOption(diff.first, [game, &diff, DifficultyMenu]() {
			game->SetDifficulty(diff.second);
			DifficultyMenu->SetCaption("Selected: " + diff.first);
		}));
	}
	DifficultyMenu->AddChild(new BackOption());
	return DifficultyMenu;
}