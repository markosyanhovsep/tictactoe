#include "MenuNode.h"

MenuNode::MenuNode(std::string&& _name):
	name(std::move(_name))
{}

MenuNode::MenuNode(const std::string& _name) :
	name(_name)
{}