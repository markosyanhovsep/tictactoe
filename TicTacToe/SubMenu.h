#pragma once
#include "MenuNode.h"

class SubMenu : public MenuNode
{
public:
	SubMenu(std::string&& _name);
	
	MenuNode* AddChild(MenuNode* node);
	MenuNode* GetChild(int index) { return children[index]; };
	void Draw();
	bool IsValidAction(int action) { return action >= 0 && action < children.size(); };
	void SetCaption(std::string&& _caption);

	virtual void Action() override;
	virtual ~SubMenu();

private:

	std::vector<MenuNode*> children;
	std::string caption;
};

